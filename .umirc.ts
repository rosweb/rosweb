import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  title: 'RosWeb',
  routes: [
    { path: '/', component: '@/pages/index' },
  ],
  fastRefresh: {},
});
