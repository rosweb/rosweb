export default (props) => {

  const classes = 'rosweb-box' + (props.className ? ` ${props.className}` : '');
  const styles = props.style || {};

  if (props.p) styles.padding = `${props.p}px`;
  if (props.pt) styles.paddingTop = `${props.pt}px`;
  if (props.pr) styles.paddingRight = `${props.pr}px`;
  if (props.pb) styles.paddingBottom = `${props.pb}px`;
  if (props.pl) styles.paddingLeft = `${props.pl}px`;
  if (props.m) styles.margin = `${props.m}px`;
  if (props.mt) styles.marginTop = `${props.mt}px`;
  if (props.mr) styles.marginRight = `${props.mr}px`;
  if (props.mb) styles.marginBottom = `${props.mb}px`;
  if (props.ml) styles.marginLeft = `${props.ml}px`;
  if (props.w) styles.width = `${props.w}`;
  if (props.h) styles.height = `${props.h}px`;
  if (props.align) styles.textAlign = `${props.align}`;
  if (props.color) styles.color = `${props.color}`;
  if (props.bgColor) styles.background = `${props.bgColor}`;

  return (
    <div style={styles} className={classes}>
      {props.children}
    </div>
  );
};
