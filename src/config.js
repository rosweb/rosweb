/**
 * Application configuration
 */
const config = {
  ros_ws: 'ws://localhost:9090',
}

/**
 * Module exports
 */
export default config;