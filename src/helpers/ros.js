import { Ros, Topic, TFClient } from 'roslib';
import config from '@/config';

class RosWeb {
  constructor() {
    this.ros = new Ros({ url: config.ros_ws });

    this.ros.on('connection', () => console.log('Connected to ROS!'));
    this.ros.on('error', (er) => console.error('Cannot connect to ROS: ', er));
    this.ros.on('close', () => console.log('ROS connection closed!'));

    this.tf = new TFClient({
      ros: this.ros,
      fixedFrame: 'map',
    });

    // =========
    // TOPICS
    // =========

    this.mapTopic = new Topic({
      ros: this.ros,
      name: "/map",
      messageType: 'nav_msgs/OccupancyGrid',
      compression: 'png'
    });

    // =========
    // SERVICES
    // =========

  }

  // =========
  // FUNCTIONS
  // =========

}

export default RosWeb;