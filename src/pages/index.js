import './index.less';
import { Link } from 'umi';
import { Layout, Typography, Space, Button } from 'antd';
import Box from '../components/Box';
import RosWeb from '@/helpers/ros';
import { Stage, Layer, Image, Group, Circle, RegularPolygon } from 'react-konva';
import useImage from 'use-image';
import map from '/public/map.jpg'

const { Header, Content } = Layout;

const rosweb = new RosWeb();

const openMessage = () => {
  message.info({ content: 'Start Mapping!', duration: 2 });
};

const Map = () => {
  const [image] = useImage(map);
  return <Image image={image} x={300} />
};

const Bot = () => {
  return <Group scaleX={0.8} scaleY={0.8} x={600} y={120}>
    <Circle radius={17} fill="#007afb" opacity={0.2} />
    <Circle radius={9} fill="white" />
    <Circle radius={6} fill="#007afb" />
  </Group>
}

const Marker = () => {
  return (
    <Group scaleX={0.8} scaleY={0.8} x={400} y={150} style={{ cursor: 'pointer' }}>
      <Circle x={0} y={24} radius={3} scaleX={2} fill="#aaa" />
      <Circle radius={15} fill="#007afb" />
      <Circle radius={10} fill="white" />
      <RegularPolygon
        x={0}
        y={17}
        radius={5}
        scaleX={1.6}
        scaleY={1.5}
        fill="#007afb"
        rotation={180}
        sides={3}
      />
    </Group>
  );
};

export default function IndexPage() {
  return (
    <Layout>
      <Header>
        <div className="layout-center">
          <div className="logo">
            <Link to="/" style={{ fontSize: 18, color: 'white' }}>
              RosWeb
            </Link>
          </div>
        </div>
      </Header>
      <Content>
        <div className="layout-center">
          <Box pt={40}>
            <Typography.Title level={1}>
              Welcome to RosWeb!
            </Typography.Title>
          </Box>
          <Box pt={20}>
            <Space direction='horizontal'>
              <Button type="primary" size="large" onClick={openMessage}>
                Start Mapping
              </Button>
            </Space>
          </Box>
          <Box mt={20} mb={40} pt={10} pb={10} bgColor="#e6e6e6" align='center' style={{ position: 'relative', overflow: 'hidden' }}>
            <Stage width={window.innerWidth - 143} height={580}>
              <Layer>
                <Map />
                <Bot />
                <Marker />
              </Layer>
            </Stage>
          </Box>
        </div>
      </Content>
    </Layout>
  );
}
